# LLM Question-Answering App

## Overview

This Streamlit-based Question-Answering (QA) application utilizes Langchain with OpenAI embeddings and Chroma vector store for document processing and retrieval 

## Getting Started

1. Clone the repository: 

```bash
git clone https://gitlab.com/genai-poc/file-convo-ai-p1.git
```

2. Ensure you have the required dependencies installed: 

```bash
pip install langchain openai pypdf tiktoken chromadb python-dotenv streamlit
```

## Usage 

**1. OpenAI API Key:**

Enter your OpenAI API key in the provided text input field on the sidebar.

**2. Upload a File:**

Upload a document in PDF, DOCX, or TXT format using the file uploader.

**3. Add Data:**

Click the "Add Data" button to read, chunk, and embed the uploaded file.

**4. Ask a Question:**

Enter a question related to the content of the uploaded file in the text input field.

**5. View Answer:**

LangChain Question-Answering model provides an answer, displayed under "LLM Answer."

**6. Chat History:**

The application maintains a chat history, displayed in the text area.

## Running the application

Execute the following command in your terminal:

```bash
streamlit run app.py
```
